<?php

namespace App\Http\Controllers;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    //
	public function index()
	{
		$tasks = Task::all();
		return view('welcome', compact('tasks'));
	}
	public function store(Request $request)
	{	
		$task = new Task;
		$task->name = $request->name;
		$task->save()
		return redirect()->action('TaskController@index');	
	}
	public function del($id)
	{
		$task = Task::find($id);
		$task->delete();
		return redirect()->action('TaskController@index');
	}
}
